import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { Post } from '../post.model';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit, OnDestroy {
 
  posts: Post[] = [];
  isLoading: boolean;
  private postsSub: Subscription;

  constructor(private _posts: PostsService) {}

  ngOnInit() {
    this.isLoading = true;
    this._posts.getPosts();
    this.postsSub = this._posts.getPostUpdateListener()
      .subscribe((posts: Post[]) => {
        this.posts = posts;
        this.isLoading = false;
      });
  }

  ngOnDestroy() {
    this.postsSub.unsubscribe();
  }

  onDeletePost(guid: string) {
    this._posts.deletePost(guid);
  }
}

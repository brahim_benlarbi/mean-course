export interface Post {
  guid: string;
  title: string;
  content: string;
  imagePath: string;
}

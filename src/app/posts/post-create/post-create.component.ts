import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';

import { PostsService } from '../posts.service';
import { Post } from '../post.model';
import { mimeType } from './mime-type.validator';

@Component({
  selector: 'app-post-create',
  templateUrl: './post-create.component.html',
  styleUrls: ['./post-create.component.css']
})
export class PostCreateComponent implements OnInit {
  guid: string;
  isLoading: boolean;
  postForm: FormGroup;
  imagePreview: string | ArrayBuffer;

  private mode: string;

  constructor(private _posts: PostsService, private _route: ActivatedRoute) { }

  ngOnInit(): void {
    this.postForm = new FormGroup({
      title: new FormControl(null, { validators: [Validators.required, Validators.minLength(3)] }),
      content: new FormControl(null, { validators: [Validators.required] }),
      image: new FormControl(null, { validators: [Validators.required], asyncValidators: [mimeType] })
    });
    this._route.paramMap.subscribe((params: ParamMap) => {
      if (params.has('guid')) {
        this.isLoading = true;
        this.guid = params.get('guid');
        this._posts.getPost(this.guid)
          .subscribe((post: Post) => {
            this.postForm.setValue({
              title: post.title,
              content: post.content,
              image: post.imagePath
            });
            this.isLoading = false;
            this.imagePreview = post.imagePath;
          });
        this.mode = 'edit';
      } else {
        this.mode = 'create';
      }
    });
  }

  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.postForm.patchValue({ image: file });
    this.postForm.get('image').updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result;
    };
    reader.readAsDataURL(file);
  }

  onSavePost() {
    if (this.postForm.invalid) {
      return;
    }

    this.isLoading = true;

    if (this.mode === 'create') {
      this._posts.addPost(this.postForm.value.title, this.postForm.value.content, this.postForm.value.image);
    } else if (this.mode === 'edit') {
      this._posts.updatePost(this.guid, this.postForm.value.title, this.postForm.value.content, this.postForm.value.image);
    }

    this.postForm.reset();
  }
}

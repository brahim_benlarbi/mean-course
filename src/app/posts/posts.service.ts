import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

import { Post } from './post.model';

@Injectable({ providedIn: 'root' })
export class PostsService {
  private posts: Post[] = [];
  private postsUpdated = new Subject<Post[]>();
  private url = '/api/posts';

  constructor(private _http: HttpClient, private _router: Router) { }

  getPostUpdateListener() {
    return this.postsUpdated.asObservable();
  }

  getPosts() {
    this._http.get<{ message: string, posts: any }>(this.url)
      .pipe(
        map(postData => {
          return postData.posts.map(post => {
            return {
              title: post.title,
              content: post.content,
              imagePath: post.imagePath,
              guid: post._id
            };
          });
        })
      )
      .subscribe((postsData) => {
        this.posts = postsData;
        this.postsUpdated.next([...this.posts]);
      });
  }

  addPost(title: string, content: string, image: File) {
    const postData = new FormData();
    postData.append('title', title);
    postData.append('content', content);
    postData.append('image', image, title);
    this._http.post<{ post: Post, message: string }>(this.url, postData)
      .subscribe(resp => {
        const post: Post = resp.post;
        this.posts.push(post);
        this.postsUpdated.next([...this.posts]);
        this._router.navigate(['/']);
      });
  }

  getPost(guid: string) {
    const url = this.url + '/' + guid;
    return this._http.get(url);
  }

  updatePost(guid: string, title: string, content: string, image: File | string) {
    const url = this.url + '/' + guid;
    let postData: Post | FormData;

    if (typeof (image) === 'object') {
      postData = new FormData();
      postData.append('guid', guid);
      postData.append('title', title);
      postData.append('content', content);
      postData.append('image', image, title);
    } else {
      postData = {
        guid: guid,
        title: title,
        content: content,
        imagePath: image
      }
    }

    this._http.put(url, postData)
      .subscribe((resp: {message: string, imagePath: string}) => {
        let uPost = this.posts.find(post => post.guid === guid);
        uPost.title = title;
        uPost.content = content;
        uPost.imagePath = resp.imagePath;
        this.postsUpdated.next([...this.posts]);
        this._router.navigate(['/']);
      });
  }

  deletePost(guid: string) {
    const url = this.url + '/' + guid;
    this._http.delete<{ message: string }>(url)
      .subscribe(resp => {
        this.posts = this.posts.filter((post) => post.guid !== guid);
        this.postsUpdated.next([...this.posts]);
      });
  }

}

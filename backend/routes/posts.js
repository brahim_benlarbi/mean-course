const express = require('express');
const Post = require('../models/post');
const multer = require('multer');

const router = express.Router();

MIME_TYPE_MAP = {
  'image/png': 'png',
  'image/jpeg': 'jpg',
  'image/jpg': 'jpg',
  'image/gif': 'gif',
}

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    const isValid = MIME_TYPE_MAP[file.mimetype];
    let error = new Error('Type is invalid');
    if(isValid) {
      error = null;
    } 
    cb(error, './backend/images');
  },
  filename: (req, file, cb) => {
    const name = file.originalname.toLocaleLowerCase().split(' ').join('-');
    const ext = MIME_TYPE_MAP[file.mimetype];
    cb(null, name + '-' + Date.now() + '.' + ext);
  }
});

router.route('')

  .post(multer({storage: storage}).single('image'), (req, res, next) => {
    const url = req.protocol + '://' + req.get('host');
    const post = new Post({
      title: req.body.title,
      content: req.body.content,
      imagePath: url + '/images/' + req.file.filename
    });
    post.save().then(cPost => {
      res.status(201).json({
        message: 'Post created sucessfully',
        post: { ...cPost, guid: cPost._id }
      });
    }).catch(error => {
      console.log(error);
    });
  })

  .get((req, res, next) => {
    Post.find({}, {
      __v: false
    }).then(docs => {
      res.status(200).json({
        message: 'Posts fetched successfully!',
        posts: docs
      });
    })

  });

router.route('/:guid')

  .get((req, res, next) => {
    const guid = req.params.guid;
    Post.findOne({
      _id: guid
    }, {
      __v: false
    }).then(doc => {
      if (doc) {
        res.status(200).json(doc);
      } else {
        res.status(404).json({
          message: 'Post not found'
        });
      }
    });
  })

  .put(multer({storage: storage}).single('image'), (req, res, next) => {
    const guid = req.params.guid;
    let imagePath = req.body.imagePath;
    if (req.file) {
      const url = req.protocol + '://' + req.get('host');
      imagePath = url + '/images/' + req.file.filename
    }
    const post = new Post({
      _id: req.body.guid,
      title: req.body.title,
      content: req.body.content,
      imagePath: imagePath
    });
    Post.updateOne({ _id: guid }, post).then(result => {
      res.status(200).json({
        message: 'The post is successfully updated',
        imagePath: imagePath
      });
    });
  })

  .delete((req, res, next) => {
    const guid = req.params.guid;
    Post.deleteOne({
      _id: guid
    }).then(() => {
      res.status(200).json({
        message: 'The post is successfully deleted'
      });
    });
  });

module.exports = router;

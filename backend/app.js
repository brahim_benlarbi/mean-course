const path = require('path')
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const pstRoutes = require('./routes/posts');

// mongoose.connect('mongodb://127.0.0.1:27017/mean-course-db')
mongoose.connect('mongodb+srv://godyr:q4eEkp5Zxp6836Gs@cluster0-z8dzr.mongodb.net/mean-course-db?retryWrites=true&w=majority')
  .then(() => {
    console.log('Connected to databae!!');
  })
  .catch(() => {
    console.error('Connection failed!!');
  });

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));

app.use('/images', express.static(path.join('backend/images')));

app.use('/api/posts', pstRoutes);

// app.use((req, res, next) => {
//   res.header('Access-Control-Allow-Origin', 'http://localhost:4200');
//   res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
//   res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, OPTIONS');
//   next();
// });

module.exports = app;
